package org.lazada.testcases;

import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.lazada.pageobjects.CartPage;
import org.lazada.pageobjects.HomePage;
import org.lazada.pageobjects.ProductDetailPage;
import org.lazada.pageobjects.SignUpPage;
import org.lazada.pageobjects.Site;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestCase {
	static WebDriver driver;
	
	@BeforeClass
	public static void initilization() {
		DesiredCapabilities cap = new DesiredCapabilities();
		driver = new FirefoxDriver(cap);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		driver.manage().window().maximize();
	}
	
	@AfterClass
	public static void cleanUp() {
		driver.close();
	}
	
	@Test 
	public void TC_001() {		
		Site site = new Site(driver);
		HomePage homePage =  site.navigatetoPSite();
		
		SignUpPage signUpPage = homePage.navigateSignUp();
		
		signUpPage.signUpError();
	}
	
	@Test
	public void TC_002() {
		Site site = new Site(driver);
		HomePage homePage =  site.navigatetoPSite();
		
		homePage.searchProduct("ipad");
		ProductDetailPage productPage = homePage.selectProduct(1);
		
		String productDetailName = productPage.getName();
		String productDetailPrice = productPage.getPrice();
		
		CartPage cart = productPage.addToCart();
		
		String productCartName = cart.getName();
		String productCartPrice = cart.getPrice();
		
		Assert.assertEquals(productCartName, productDetailName);
		Assert.assertEquals(productCartPrice, productDetailPrice);
		
	}
	
	@Test
	public void TC_003() {
		Site site = new Site(driver);
		HomePage homePage =  site.navigatetoPSite();
		
		homePage.searchProduct("ipad");
		ProductDetailPage productPage = homePage.selectProduct(3);
		
		CartPage cart = productPage.addToCart();
		 
		driver.findElement(By.cssSelector("#cartContinueShopping")).click();
		(new WebDriverWait(driver, 20)).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("form#cart-items-list-form")));
		driver.navigate().back();
		
		productPage = homePage.selectProduct(0);
		
		cart = productPage.addToCart();
		
		cart.selectQuantity(3);
		
		(new WebDriverWait(driver, 20))
		  .until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.nyroModalCont.nyroModalLoad")));
		
		Assert.assertTrue("Total price is wrong", cart.getFinalPrice() == cart.getTotalPrice());
	}

}
