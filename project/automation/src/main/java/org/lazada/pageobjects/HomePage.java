package org.lazada.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {

	WebDriver driver;
	
	private static String txtSearch = "input#searchInput";
	private static String btnSearch = "div.header__search__submit";
	private static String buttonSignUp = "li.header-authentication-popup";
	private static String product = "div.product-card__img";
	private static String buttonAddToCart = "span.button-buy__text";
	private static String gotoCart = "i.icon.icon-cart-white";
	private static String buttonAddCart = "button#AddToCart";
	
	public HomePage(WebDriver driver) {
		this.driver = driver;
	}
	
	public SignUpPage navigateSignUp() {
		SignUpPage signUp = new SignUpPage(driver);
		
		driver.findElements(By.cssSelector(buttonSignUp)).get(1).click();
		
		return signUp;
	}
	
	public CartPage navigateToCart() {
		(new WebDriverWait(driver, 10))
		   .until(ExpectedConditions.elementToBeClickable(driver.findElement(By.cssSelector(gotoCart))));
		
		driver.findElement(By.cssSelector(gotoCart)).click();
		
		return new CartPage(driver);
	}
	
	public ProductDetailPage selectProduct(int i) {
		(new WebDriverWait(driver, 30))
				   .until(ExpectedConditions.elementToBeClickable(driver.findElements(By.cssSelector(product)).get(i)));
		
		driver.findElements(By.cssSelector(product)).get(i).click();
		
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(buttonAddCart)));
		
		return new ProductDetailPage(driver);
	}
	
	public void searchProduct(String keyword) {
		driver.findElement(By.cssSelector(txtSearch)).sendKeys(keyword);
		driver.findElement(By.cssSelector(btnSearch)).click();
	}
	
	public void addProductToCart(int i ) {
		(new WebDriverWait(driver, 10))
		   .until(ExpectedConditions.elementToBeClickable(driver.findElements(By.cssSelector(product)).get(i)));

		Actions actions = new Actions(driver);
		actions.moveToElement(driver.findElements(By.cssSelector(product)).get(i));
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		actions.click(driver.findElement(By.cssSelector(buttonAddToCart)));
		actions.build().perform();
		
		/*(new WebDriverWait(driver, 10))
		   .until(ExpectedConditions.elementToBeClickable(driver.findElement(By.cssSelector(buttonAddToCart))));
		
		driver.findElement(By.cssSelector(buttonAddToCart)).click();*/
	}

}
