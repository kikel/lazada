package org.lazada.pageobjects;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SignUpPage {
	WebDriver driver;
	
	private String buttonSubmit = "button#send";
	private String spanError = "span.error-display";
	
	public SignUpPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public void signUpError() {		
		driver.findElement(By.cssSelector(buttonSubmit)).click();
		
		Assert.assertTrue("Wrong validate", driver.findElements(By.cssSelector(spanError)).size() == 4);
	}
}
