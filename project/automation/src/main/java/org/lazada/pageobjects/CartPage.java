package org.lazada.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CartPage {
	
	WebDriver driver;
	
	private static String name = "div.productdescription";
	private static String price  = "td.width_15.price.center";
	private static String itemPrice = "td.width_20.right_align.price.lastcolumn";
	private static String totalPrice = "td.width_30.right_align.lastcolumn";
	private static String selectQuantity = "select";
	private static String tableList = "form#cart-items-list-form table.width_100.producttable";
	
	public CartPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public String getName() {
		WebElement element = (new WebDriverWait(driver, 10))
				   .until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(name)));
		String temp = element.getText();
		System.out.println(temp);
		return temp;
		
	}
	
	public String getPrice() {
		String temp = driver.findElements(By.cssSelector(price)).get(0).findElements(By.cssSelector("span")).get(0).getText();
		System.out.println(temp);
		return temp;
	}
	
	public double getAllPrice() {
		double num = 0;
		(new WebDriverWait(driver, 10))
				   .until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(price)));
		
		for (WebElement temp : driver.findElements(By.cssSelector(price))) {
			System.out.println(temp.findElements(By.cssSelector("span")).get(0).getText());
			String txtx = temp.findElements(By.cssSelector("span")).get(0).getText();
			num = num + Double.parseDouble(txtx.replace(".", "").replace("VND", "").trim());
		}
		
		System.out.println(num);
		
		return num;
	}
	
	public double getFinalPrice() {
		double num = 0;
		(new WebDriverWait(driver, 10))
		   .until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(itemPrice)));
		for (WebElement temp : driver.findElements(By.cssSelector(itemPrice))) {
			String txtx = temp.getText();
			num = num + Double.parseDouble(txtx.replace(".", "").replace("VND", "").trim()); 
		}
		
		System.out.println("Final Price: " + num);
		
		return num;
	}
	
	public double getTotalPrice() {
		String temp = driver.findElement(By.cssSelector(totalPrice)).getText();
		double num = Double.parseDouble(temp.replace(".", "").replace("VND", "").trim());
		
		System.out.println("Total Price: " + num);
		
		return num;
	}
	
	public void selectQuantity(int quantity) {
//		String oldText = driver.findElements(By.cssSelector(tableList)).get(0).findElement(By.cssSelector(itemPrice)).getText();
//		System.out.println(oldText);		
		
		Select comSelect = new Select(driver.findElements(By.cssSelector(tableList)).get(0).findElement(By.cssSelector(selectQuantity)));
		comSelect.selectByVisibleText(String.valueOf(quantity));
//		comSelect.selectByValue(String.valueOf(quantity));
		
		
//		String newText = driver.findElements(By.cssSelector(tableList)).get(0).findElement(By.cssSelector(itemPrice)).getText();
//		System.out.println(newText);
		
	}
	
}
