package org.lazada.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProductDetailPage {
	WebDriver driver;
	
	private static String buttonAddCart = "button#AddToCart";
	private static String price = "span#special_price_box";
	private static String name = "h1#prod_title";
	
	public ProductDetailPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public CartPage addToCart() {
//		System.out.println(driver.findElements(By.cssSelector(buttonAddCart)).size());
		(new WebDriverWait(driver, 20)).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(buttonAddCart)));
		
		CartPage cart = new CartPage(driver);
		driver.findElement(By.cssSelector(buttonAddCart)).click();
		
		(new WebDriverWait(driver, 20)).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("form#cart-items-list-form")));
		
		return cart;
	}
	
	public String getPrice() {
		String temp = driver.findElement(By.cssSelector(price)).getText();
		System.out.println(temp);
		return temp;
	}
	
	public String getName() {
		String temp = driver.findElement(By.cssSelector(name)).getText();
		System.out.println(temp);
		return temp;
	}
}
