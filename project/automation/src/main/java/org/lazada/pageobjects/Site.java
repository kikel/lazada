package org.lazada.pageobjects;

import org.openqa.selenium.WebDriver;

public class Site {
	
	private static String SITE_P = "http://www.lazada.vn";
	WebDriver driver;
	
	public Site(WebDriver driver) {
		this.driver = driver;
	}
	
	public HomePage navigatetoPSite() {
		HomePage homePage = new HomePage(driver);
		driver.navigate().to(SITE_P);
		
		return homePage;
	}

}
